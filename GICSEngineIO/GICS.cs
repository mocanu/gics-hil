﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Windows.Media;

namespace GICSEngineIO
{
    class GICS
    {
        public enum Mode
        {
            GICS_MODE_REMOTE = 0,
            GICS_MODE_LOCAL = 1
        };

        public const byte GICS_API = 0;
        public const byte GICS_IED = 1;

        public const byte GICS_MAGIC = 0xd0;
        public const byte GICS_READ = 0x01;
        public const byte GICS_WRITE = 0x02;
        public const byte GICS_DISCRETE = 0x04;
        public const byte GICS_ANALOG = 0x08;
        public const byte GICS_DA = 0x10;
        public const byte GICS_READ_HOLDS = 0x20;
        public const byte GICS_PARAMS = 0x80;
        public static bool udpstat;
        public class Transaction
        {
            public byte function;
            public byte magic;
            public ushort[] analogs;
            public bool[] digitals;

            public Transaction(byte function, ushort[] analogs, bool[] digitals)
            {
                this.function = function;
                magic = GICS_MAGIC;
                this.analogs = analogs;
                this.digitals = digitals;
            }
            public byte[] ToBytes()
            {
                int length = 4;
                if (((function | GICS_ANALOG) != 0 || (function | GICS_DA) != 0) && analogs != null)
                    length += analogs.Length * 2;
                if (((function | GICS_DISCRETE) != 0 || (function | GICS_DA) != 0) && digitals != null)
                    length += (digitals.Length - 1) / 8 + 1;
                byte[] data = new byte[length];
                length -= 4;
                int i = 0;
                data[i++] = function;
                data[i++] = magic;
                data[i++] = LoByte((ushort)length);
                data[i++] = HiByte((ushort)length);
                if (((function | GICS_ANALOG) != 0 || (function | GICS_DA) != 0) && analogs != null)
                    for (int j = 0; j < analogs.Length; j++)
                    {
                        data[i++] = LoByte(analogs[j]);
                        data[i++] = HiByte(analogs[j]);
                    }
                if (((function | GICS_DISCRETE) != 0 || (function | GICS_DA) != 0) && digitals != null)
                    for (int j = 0; j < digitals.Length; j++)
                    {
                        data[i] |= (byte)(digitals[j] ? 1 << (j % 8) : 0);
                        if ((j + 1) % 8 == 0)
                            i++;
                    }
                return data;
            }
            public static Transaction FromBytes(byte[] data)
            {
                if (data.Length < 22)
                    return null;
                int i = 0;
                byte function = data[i++];
                byte magic = data[i++];
                if (magic != GICS_MAGIC + 1)
                    return null;
                ushort length = Word(data[i], data[i + 1]);
                i += 2;
                if (length < 18)
                    return null;
                ushort[] analogs = new ushort[8];
                bool[] digitals = new bool[16];
                if ((function | GICS_ANALOG) != 0 || (function | GICS_DA) != 0)
                    for (int j = 0; j < analogs.Length; j++)
                    {
                        analogs[j] = Word(data[i], data[i + 1]);
                        i += 2;
                    }
                if ((function | GICS_DISCRETE) != 0 || (function | GICS_DA) != 0)
                    for (int j = 0; j < digitals.Length; j++)
                    {
                        digitals[j] = (data[i] & (1 << (j % 8))) != 0;
                        if ((j + 1) % 8 == 0)
                            i++;
                    }
                return new Transaction(function, analogs, digitals);
            }
            static byte LoByte(ushort n)
            {
                return (byte)(n >> 8);
            }
            static byte HiByte(ushort n)
            {
                return (byte)(n & 0xFF);
            }
            static ushort Word(byte lo, byte hi)
            {
                return (ushort)((lo << 8) | hi);
            }
        }
       
        public static void ReadDA(string ip, out ushort[] analogs, out bool[] digitals)
        {
            analogs = new ushort[8];
            digitals = new bool[16];
            Transaction query = new Transaction(GICS_READ | GICS_DA, null, null);
            byte[] data = query.ToBytes();
            try
            {
                using (UdpClient udp = new UdpClient())
                {
                    udp.Client.SendTimeout = 2000;
                    udp.Client.ReceiveTimeout = 2000;
                    IPEndPoint ep = new IPEndPoint(IPAddress.Parse(ip), 2015);
                    udp.Connect(ep);
                    try
                    {
                        udpstat = false;
                        udp.Send(data, data.Length);

                    }
                    catch
                    {
                        udpstat=true;
                    }
                    data = udp.Receive(ref ep);
                    Transaction response = Transaction.FromBytes(data);
                    if (response != null)
                    {
                        analogs = response.analogs;
                        digitals = response.digitals;
                    }
                }
            }
            catch (Exception)
            {
               udpstat=true;
            }
        }
       
        public static void WriteDA(string ip, ushort[] analogs, bool[] digitals)
        {
            Transaction query = new Transaction(GICS_WRITE | GICS_DA, analogs, digitals);
            byte[] data = query.ToBytes();
            try
            {
                /*
                IPAddress ipAddress = Dns.GetHostAddresses(ip)[0];
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, 2015);
                using (Socket s = new Socket(ipEndPoint.AddressFamily, SocketType.Dgram, ProtocolType.Udp))
                    s.SendTo(data, data.Length, 0, ipEndPoint);
                */
                using (UdpClient udp = new UdpClient(ip, 2015))
                    udp.Send(data, data.Length);
            }
            catch (Exception)
            {
            }
        }
    }
}
