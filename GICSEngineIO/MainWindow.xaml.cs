﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.ComponentModel;
using System.Windows.Threading;
using System.Windows.Media;

namespace GICSEngineIO
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MemoryIOs memoryIOs = new MemoryIOs();
        DispatcherTimer timer = new DispatcherTimer();
        bool locked;

        public MainWindow()
        {
            InitializeComponent();
            inputsList.ItemsSource = memoryIOs.inputs;
            outputsList.ItemsSource = memoryIOs.outputs;
            timer.Tick += new System.EventHandler(OnTimer);
            timer.Interval = new System.TimeSpan(0, 0, 1);
        }

        private void OnTimer(object sender, EventArgs e)
        {
            OnRefresh(null, null);
        }

        private void OnClose(object sender, CancelEventArgs e)
        {
            memoryIOs.Stop();
        }

        private void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (e.PreviousSize.Width != 0 && e.PreviousSize.Height != 0)
            {
                double dw = e.NewSize.Width - e.PreviousSize.Width;
                double dh = e.NewSize.Height - e.PreviousSize.Height;
                inputsList.Width += dw;
                inputsList.Height += dh / 2;
                outputsText.Margin = new Thickness(outputsText.Margin.Left, outputsText.Margin.Top + dh / 2, 0, 0);
                outputsList.Margin = new Thickness(outputsList.Margin.Left, outputsList.Margin.Top + dh / 2, 0, 0);
                outputsList.Width += dw;
                outputsList.Height += dh / 2;
                openBtn.Margin = new Thickness(openBtn.Margin.Left, openBtn.Margin.Top + dh, 0, 0);
                saveBtn.Margin = new Thickness(saveBtn.Margin.Left, saveBtn.Margin.Top + dh, 0, 0);
                refreshBtn.Margin = new Thickness(refreshBtn.Margin.Left, refreshBtn.Margin.Top + dh, 0, 0);
                connectBtn.Margin = new Thickness(connectBtn.Margin.Left, connectBtn.Margin.Top + dh, 0, 0);
                disconnectBtn.Margin = new Thickness(disconnectBtn.Margin.Left, disconnectBtn.Margin.Top + dh, 0, 0);
                periodText.Margin = new Thickness(periodText.Margin.Left, periodText.Margin.Top + dh, 0, 0);
                periodEdit.Margin = new Thickness(periodEdit.Margin.Left, periodEdit.Margin.Top + dh, 0, 0);
                ipEdit.Margin = new Thickness(ipEdit.Margin.Left, ipEdit.Margin.Top + dh, 0, 0);
                orderEdit.Margin = new Thickness(orderEdit.Margin.Left, orderEdit.Margin.Top + dh, 0, 0);
                minEdit.Margin = new Thickness(minEdit.Margin.Left, minEdit.Margin.Top + dh, 0, 0);
                maxEdit.Margin = new Thickness(maxEdit.Margin.Left, maxEdit.Margin.Top + dh, 0, 0);
            }
        }

        private void OnOpen(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Open configuration file";
            ofd.Filter = "Configuration files|*.xml|All files|*.*";
            if (ofd.ShowDialog() == true)
            {
                double period;
                memoryIOs.Open(ofd.FileName, out period);
                periodEdit.Text = period.ToString();
                inputsList.Items.Refresh();
                outputsList.Items.Refresh();
            }
        }

        private void OnQuit(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OnSave(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Save configuration file";
            sfd.Filter = "Configuration files|*.xml|All files|*.*";
            sfd.DefaultExt = ".xml";
            if (sfd.ShowDialog() == true)
            {
                double period;
                double.TryParse(periodEdit.Text, out period);
                memoryIOs.Save(sfd.FileName, period);
            }
        }
        void FillRectangle(Rectangle rect, bool digital)
        {
            rect.Fill = digital ? new SolidColorBrush(Color.FromRgb(255, 0, 0)) : new SolidColorBrush(Color.FromRgb(0, 255, 255));
        }

        private void OnRefresh(object sender, RoutedEventArgs e)
        {
            memoryIOs.Refresh();
            inputsList.Items.Refresh();
            outputsList.Items.Refresh();
            FillRectangle(St, GICS.udpstat);
        }
        private void OnConnect(object sender, RoutedEventArgs e)
        {
            openBtn.IsEnabled = false;
            connectBtn.Visibility = System.Windows.Visibility.Hidden;
            disconnectBtn.Visibility = System.Windows.Visibility.Visible;
            periodEdit.IsEnabled = false;
            ipEdit.IsEnabled = false;
            orderEdit.IsEnabled = false;
            minEdit.IsEnabled = false;
            maxEdit.IsEnabled = false;
            double period;
            double.TryParse(periodEdit.Text, out period);
            if (double.TryParse(periodEdit.Text, out period) && period > 0)
                memoryIOs.Connect(period);
            timer.Start();
        }

        private void OnDisconnect(object sender, RoutedEventArgs e)
        {
            timer.Stop();
            memoryIOs.Disconnect();
            openBtn.IsEnabled = true;
            connectBtn.Visibility = System.Windows.Visibility.Visible;
            disconnectBtn.Visibility = System.Windows.Visibility.Hidden;
            periodEdit.IsEnabled = true;
            ipEdit.IsEnabled = true;
            orderEdit.IsEnabled = true;
            minEdit.IsEnabled = true;
            maxEdit.IsEnabled = true;
        }

        private void OnList(object sender, SelectionChangedEventArgs e)
        {
            if (locked)
                return;
            locked = true;
            if (sender != inputsList)
                inputsList.UnselectAll();
            else
                outputsList.UnselectAll();
            locked = false;
            string ip = "";
            string order = "";
            string min = "";
            string max = "";
            foreach (MemoryIO m in inputsList.SelectedItems)
            {
                if (ip == "")
                    ip = m.ip;
                else if (ip != m.ip)
                    ip = "#";
                if (order == "")
                    order = m.Order;
                else if (order != m.Order)
                    order = "#";
                if (min == "")
                    min = m.Min;
                else if (min != m.Min)
                    min = "#";
                if (max == "")
                    max = m.Max;
                else if (max != m.Max)
                    max = "#";
            }
            foreach (MemoryIO m in outputsList.SelectedItems)
            {
                if (ip == "")
                    ip = m.ip;
                else if (ip != m.ip)
                    ip = "#";
                if (order == "")
                    order = m.Order;
                else if (order != m.Order)
                    order = "#";
                if (min == "")
                    min = m.Min;
                else if (min != m.Min)
                    min = "#";
                if (max == "")
                    max = m.Max;
                else if (max != m.Max)
                    max = "#";
            }
            locked = true;
            ipEdit.Text = ip != "#" ? ip : "";
            orderEdit.Text = order != "#" ? order : "";
            minEdit.Text = min != "#" ? min : "";
            maxEdit.Text = max != "#" ? max : "";
            locked = false;
        }

        private void OnIPEdit(object sender, TextChangedEventArgs e)
        {
            if (locked)
                return;
            string ip = ipEdit.Text;
            foreach (MemoryIO m in inputsList.SelectedItems)
                m.ip = ip;
            foreach (MemoryIO m in outputsList.SelectedItems)
                m.ip = ip;
            inputsList.Items.Refresh();
            outputsList.Items.Refresh();
        }
        private void OnOrderEdit(object sender, TextChangedEventArgs e)
        {
            if (locked)
                return;
            int order;
            int.TryParse(orderEdit.Text, out order);
            if (order == 0)
            {
                foreach (MemoryIO m1 in inputsList.SelectedItems)
                    m1.order = order;
                foreach (MemoryIO m1 in outputsList.SelectedItems)
                    m1.order = order;
            }
            else if (order > 0)
            {
                foreach (MemoryIO m1 in inputsList.SelectedItems)
                    if (m1.ip != "")
                    {
                        m1.order = order;
                        bool find;
                        do
                        {
                            find = false;
                            foreach (MemoryIO m2 in memoryIOs.inputs)
                                if (m2.ip == m1.ip && m2.Type == m1.Type && m2 != m1 && m2.order == m1.order)
                                {
                                    m1.order++;
                                    find = true;
                                    break;
                                }
                        } while (find);
                    }
                foreach (MemoryIO m1 in outputsList.SelectedItems)
                    if (m1.ip != "")
                    {
                        m1.order = order;
                        bool find;
                        do
                        {
                            find = false;
                            foreach (MemoryIO m2 in memoryIOs.outputs)
                                if (m2.ip == m1.ip && m2.Type == m1.Type && m2 != m1 && m2.order == m1.order)
                                {
                                    m1.order++;
                                    find = true;
                                    break;
                                }
                        } while (find);
                    }
            }
            foreach (MemoryIO m in memoryIOs.inputs)
                if ((m.IsDigital() && m.order > 16) || (m.IsAnalog() && m.order > 8))
                    m.order = 0;
            foreach (MemoryIO m in memoryIOs.outputs)
                if ((m.IsDigital() && m.order > 16) || (m.IsAnalog() && m.order > 8))
                    m.order = 0;
            inputsList.Items.Refresh();
            outputsList.Items.Refresh();
        }
        private void OnMinEdit(object sender, TextChangedEventArgs e)
        {
            if (locked)
                return;
            double min;
            double.TryParse(minEdit.Text, out min);
            foreach (MemoryIO m in inputsList.SelectedItems)
                m.min = min;
            foreach (MemoryIO m in outputsList.SelectedItems)
                m.min = min;
            inputsList.Items.Refresh();
            outputsList.Items.Refresh();
        }
        private void OnMaxEdit(object sender, TextChangedEventArgs e)
        {
            if (locked)
                return;
            double max;
            double.TryParse(maxEdit.Text, out max);
            foreach (MemoryIO m in inputsList.SelectedItems)
                m.max = max;
            foreach (MemoryIO m in outputsList.SelectedItems)
                m.max = max;
            inputsList.Items.Refresh();
            outputsList.Items.Refresh();
        }

        private void PeriodEdit_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
