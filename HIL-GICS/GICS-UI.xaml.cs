﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;

namespace HILGICS
{





    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public const int GICS_API = 0, GICS_IED = 1;
        public const int GICS_MAGIC = 0xd0;
        public const int GICS_READ = 0x01, GICS_WRITE = 0x02, GICS_DISCRETE = 0x04, GICS_ANALOG = 0x08, GICS_DA = 0x10, GICS_READ_HOLDS = 0x20, GICS_PARAMS = 0x80;
        Socket s;
        EndPoint ipe;
        byte[] message_req, message_answ ;
        public ushort[] analogs;
        public bool[] digitals;

        public struct GICSTransaction
        {
            public byte function;
            public byte magic;
            public ushort length;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 255, ArraySubType = UnmanagedType.U2)]
            public ushort[] data;
        }

        byte[] getBytes(GICSTransaction str)
        {
            int size = Marshal.SizeOf(str);
            byte[] arr = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(str, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);
            return arr;
        }


        public class Transaction
        {
            public byte function;
            public byte magic;
            public ushort[] analogs;
            public bool[] digitals;

            public Transaction(byte function, ushort[] analogs, bool[] digitals)
            {
                this.function = function;
                magic = GICS_MAGIC;
                this.analogs = analogs;
                this.digitals = digitals;
            }
            public byte[] ToBytes()
            {
                int length = 4;
                if (((function | GICS_ANALOG) != 0 || (function | GICS_DA) != 0) && analogs != null)
                    length += analogs.Length * 2;
                if (((function | GICS_DISCRETE) != 0 || (function | GICS_DA) != 0) && digitals != null)
                    length += (digitals.Length - 1) / 8 + 1;
                byte[] data = new byte[length];
                length -= 4;
                int i = 0;
                data[i++] = function;
                data[i++] = magic;
                data[i++] = LoByte((ushort)length);
                data[i++] = HiByte((ushort)length);
                if (((function | GICS_ANALOG) != 0 || (function | GICS_DA) != 0) && analogs != null)
                    for (int j = 0; j < analogs.Length; j++)
                    {
                        data[i++] = LoByte(analogs[j]);
                        data[i++] = HiByte(analogs[j]);
                    }
                if (((function | GICS_DISCRETE) != 0 || (function | GICS_DA) != 0) && digitals != null)
                    for (int j = 0; j < digitals.Length; j++)
                    {
                        data[i] |= (byte)(digitals[j] ? 1 << (j % 8) : 0);
                        if ((j + 1) % 8 == 0)
                            i++;
                    }
                return data;
            }
            public static Transaction FromBytes(byte[] data)
            {
                if (data.Length < 22)
                    return null;
                int i = 0;
                byte function = data[i++];
                byte magic = data[i++];
                if (magic != GICS_MAGIC + 1)
                    return null;
                ushort length = Word(data[i], data[i + 1]);
                i += 2;
                if (length < 18)
                    return null;
                ushort[] analogs = new ushort[8];
                bool[] digitals = new bool[16];
                if ((function | GICS_ANALOG) != 0 || (function | GICS_DA) != 0)
                    for (int j = 0; j < analogs.Length; j++)
                    {
                        analogs[j] = Word(data[i], data[i + 1]);
                        i += 2;
                    }
                if ((function | GICS_DISCRETE) != 0 || (function | GICS_DA) != 0)
                    for (int j = 0; j < digitals.Length; j++)
                    {
                        digitals[j] = (data[i] & (1 << (j % 8))) != 0;
                        if ((j + 1) % 8 == 0)
                            i++;
                    }
                return new Transaction(function, analogs, digitals);
            }
            static byte LoByte(ushort n)
            {
                return (byte)(n >> 8);
            }
            static byte HiByte(ushort n)
            {
                return (byte)(n & 0xFF);
            }
            static ushort Word(byte lo, byte hi)
            {
                return (ushort)((lo << 8) | hi);
            }
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OnUpdate(object sender, RoutedEventArgs e)
        {
            int port;
            if (TargetText.Text == "" || !int.TryParse(TargetPort.Text, out port))
                return;
            analogs = new ushort[8];
            digitals = new bool[16];
            int i = 0;
            ushort.TryParse(AO1.Text, out analogs[i++]);
            ushort.TryParse(AO2.Text, out analogs[i++]);
            ushort.TryParse(AO3.Text, out analogs[i++]);
            ushort.TryParse(AO4.Text, out analogs[i++]);
            ushort.TryParse(AO5.Text, out analogs[i++]);
            ushort.TryParse(AO6.Text, out analogs[i++]);
            ushort.TryParse(AO7.Text, out analogs[i++]);
            ushort.TryParse(AO8.Text, out analogs[i++]);
            i = 0;
            digitals[i++] = (bool)I01.IsChecked;
            digitals[i++] = (bool)I02.IsChecked;
            digitals[i++] = (bool)I03.IsChecked;
            digitals[i++] = (bool)I04.IsChecked;
            digitals[i++] = (bool)I05.IsChecked;
            digitals[i++] = (bool)I06.IsChecked;
            digitals[i++] = (bool)I07.IsChecked;
            digitals[i++] = (bool)I08.IsChecked;
            digitals[i++] = (bool)I09.IsChecked;
            digitals[i++] = (bool)I10.IsChecked;
            digitals[i++] = (bool)I11.IsChecked;
            digitals[i++] = (bool)I12.IsChecked;
            digitals[i++] = (bool)I13.IsChecked;
            digitals[i++] = (bool)I14.IsChecked;
            digitals[i++] = (bool)I15.IsChecked;
            digitals[i++] = (bool)I16.IsChecked;
    
            Transaction query = new Transaction(GICS_WRITE | GICS_DA, analogs, digitals);
            byte[] data = query.ToBytes();
            try
            {
                /*
                IPAddress ipAddress = Dns.GetHostAddresses(ip)[0];
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, port);
                using (Socket s = new Socket(ipEndPoint.AddressFamily, SocketType.Dgram, ProtocolType.Udp))
                    s.SendTo(data, data.Length, 0, ipEndPoint);
                */
                using (UdpClient udp = new UdpClient(TargetText.Text, port))
                    udp.Send(data, data.Length);
            }
            catch (Exception ex)
            {
            }
            analogs = new ushort[8];
            Thread.Sleep(100);
            query = new Transaction(GICS_READ | GICS_DA, null, null);
            data = query.ToBytes();
            try
            {
                using (UdpClient udp = new UdpClient())
                {
                    udp.Client.SendTimeout = 2000;
                    udp.Client.ReceiveTimeout = 2000;
                    IPEndPoint ep = new IPEndPoint(IPAddress.Parse(TargetText.Text), port);
                    udp.Connect(ep);
                    try
                    {
                        FillRectangle(St, false);
                        udp.Send(data, data.Length);
                        
                    }
                    catch
                    {
                        FillRectangle(St, true);
                    }
                    data = udp.Receive(ref ep);
                    Transaction response = Transaction.FromBytes(data);
                    if (response != null)
                    {
                        analogs = response.analogs;
                        digitals = response.digitals;
						i = 0;
						FillText(AI1, analogs[i++]);
						FillText(AI2, analogs[i++]);
						FillText(AI3, analogs[i++]);
						FillText(AI4, analogs[i++]);
						FillText(AI5, analogs[i++]);
						FillText(AI6, analogs[i++]);
						FillText(AI7, analogs[i++]);
						FillText(AI8, analogs[i++]);
						i = 0;
						FillRectangle(O01, digitals[i++]);
						FillRectangle(O02, digitals[i++]);
						FillRectangle(O03, digitals[i++]);
						FillRectangle(O04, digitals[i++]);
						FillRectangle(O05, digitals[i++]);
						FillRectangle(O06, digitals[i++]);
						FillRectangle(O07, digitals[i++]);
						FillRectangle(O08, digitals[i++]);
						FillRectangle(O09, digitals[i++]);
						FillRectangle(O10, digitals[i++]);
						FillRectangle(O11, digitals[i++]);
						FillRectangle(O12, digitals[i++]);
						FillRectangle(O13, digitals[i++]);
						FillRectangle(O14, digitals[i++]);
						FillRectangle(O15, digitals[i++]);
						FillRectangle(O16, digitals[i++]);
                    }
                }
            }
            catch (Exception)
            {
                FillRectangle(St, true);
            }
        }
		
		void FillRectangle(Rectangle rect, bool digital)
		{
			rect.Fill= digital ? new SolidColorBrush(Color.FromRgb(255, 0, 0)) : new SolidColorBrush(Color.FromRgb(0, 255, 255));
		}

		void FillText(TextBox text, ushort analog)
		{
			text.Text = string.Format("{0:0000}", analog);
		}


        private  void UDPSocket(string server, int port)
        {
            IPAddress[] hostEntry = null;
           

            // Get host related information.
            hostEntry = Dns.GetHostAddresses(server);

            ipe = new IPEndPoint(hostEntry[0], port);
            s = new Socket(ipe.AddressFamily, SocketType.Dgram, ProtocolType.Udp);
            s.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, 1000);
            s.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 1000);

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string host = TargetText.Text;
            ushort port;

            ushort.TryParse(TargetPort.Text, out port);
            UDPSocket(host, port);
        }
    }
}
   
