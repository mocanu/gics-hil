/*
 * gics.c
 *
 *  Created on: 2 mai 2018
 *      Author: GICS
 */

#include "gics.h"
#include <math.h>
#include <string.h>

extern SPI_HandleTypeDef hspi3;
extern I2C_HandleTypeDef hi2c1;

static int config_mode = GICS_API;

void gics_udp_server_receive_callback(void *arg, struct udp_pcb *upcb,
		struct pbuf *p_chain, const ip_addr_t *addr, u16_t port) {

	struct pbuf * p = p_chain;
	GICSTransaction * query;
	GICSTransaction response;
	while (p != NULL) {
		if (sizeof(p->payload) >= 4) {
			query = (GICSTransaction*) (p->payload);
			if((query->magic==GICS_MAGIC) && ((p->len)==(ntohs(query->length)+4)))
			{
				if (query->function & GICS_WRITE)  //Write_loop
				{  //tdb : check params
					if (query->function & GICS_ANALOG) {
						GICS_DEBUGF("Remote Analog Write");
						for (size_t iCh = 0; iCh < 8; iCh++) {
							VDAC_CTRL[iCh] = ntohs(query->data[iCh]);
							}
						gics_update_DAC_vars();
					}
					if (query->function & GICS_DISCRETE) { //tdb : check params
						GICS_DEBUGF("Write Discrete: %d\r\n",transaction->data[0]);
						//WRITE_MCP(0x40, 0x00, transaction->data[0], transaction->data[1]);
						WRITE_MCP(0x40, 0x00, ntohs(query->data[0] & 0xFF),
								(ntohs(query->data[0]) & 0xFF00) >> 8);

					}
					if (query->function & GICS_DA) { //tdb : check params
						for (size_t iCh = 0; iCh < 8; iCh++) {
							VDAC_CTRL[iCh] = ntohs(query->data[iCh]);
							}
						gics_update_DAC_vars();
						WRITE_MCP(0x40, 0x00, ntohs(query->data[8]) & 0xFF,
								(ntohs(query->data[8]) & 0xFF00) >> 8);
					}
				} //end write_loop
				if (query->function & GICS_READ) {
					response.function = query->function;
					response.magic = GICS_MAGIC + 1;
					if (query->function & GICS_DISCRETE) {
						//		copier les  16 bits I dans data[0]
						uint8_t a[2] = { 0xff, 0xff };
						READ_MCP(0x42, 0x00, a);
						response.data[0] = (a[0]) + 256 * (a[1]);
						response.length = htons(2);
					}
					if (query->function & GICS_ANALOG) {
						//	lire les 6 potantiometres (ADC) copier dans data[0] ...  data[7]
						//	conversion en continue et transfert par DMA il suffit de lire le tableau
						response.data[0] = htons(ADCxDATA[0]);
						response.data[1] = htons(ADCxDATA[1]);
						response.data[2] = htons(ADCxDATA[2]);
						response.data[3] = htons(ADCxDATA[3]);
						response.data[4] = htons(ADCxDATA[4]);
						response.data[5] = htons(ADCxDATA[5]);
						response.data[6] = htons(ADCxDATA[6]);
						response.data[7] = htons(ADCxDATA[7]);
						response.length = htons(16);
					}
					if (query->function & GICS_DA) {
						response.data[0] = htons(ADCxDATA[0]);
						response.data[1] = htons(ADCxDATA[1]);
						response.data[2] = htons(ADCxDATA[2]);
						response.data[3] = htons(ADCxDATA[3]);
						response.data[4] = htons(ADCxDATA[4]);
						response.data[5] = htons(ADCxDATA[5]);
						response.data[6] = htons(ADCxDATA[6]);
						response.data[7] = htons(ADCxDATA[7]);
						uint8_t a[2] = { 0xff, 0xff };
						READ_MCP(0x42, 0x00, a);
						response.data[8] = (a[0]) + 256 * (a[1]);
						response.length = htons(18);
					}

					struct pbuf * answer;
					answer = pbuf_alloc(PBUF_RAW, htons(response.length) + 4,
							PBUF_POOL);
					// udp_connect(upcb, addr, port);
					memcpy(answer->payload, &response,
							htons(response.length) + 4);
					int diag; /* Tell the client that we have accepted it */
					diag = udp_sendto(upcb, answer, addr, port);
					// diag=udp_send(upcb,answer );
					if (diag != ERR_OK) {
						diag = diag + 1;
					}
					/* free the UDP connection, so we can accept new clients */
					// udp_disconnect(upcb);
					pbuf_free(answer);
				}
				p = p->next;
			} //end tests
		}
	} //en packet processing
	pbuf_free(p_chain);
}

void gics_udp_server_init(void) {
	struct udp_pcb *upcb;
	err_t err;

	/* Create a new UDP control block  */
	upcb = udp_new();

	if (upcb) {
		/* Bind the upcb to the UDP_PORT port */
		/* Using IP_ADDR_ANY allow the upcb to be used by any local interface */
		err = udp_bind(upcb, IP_ADDR_ANY, GICS_UDP_SERVER_PORT);

		if (err == ERR_OK) {
			/* Set a receive callback for the upcb */
			udp_recv(upcb, gics_udp_server_receive_callback, NULL);
		} else {
			udp_remove(upcb);
		}
	}
}

/*
 * Gestion du MCP23016:
 * L'adresse est cod�e sur 7 bits dans un mot de 8bits d�cal� � gauche (cf. doc HAL)
 * Le codage du mot est : 0b0100XYZ (X=A2, Y=A1, Z=A0)
 * donc:
 *  - le MCP en sortie est � l'adresse 0x40
 *  - le MCP en entr�e est � l'adresse 0x42
 */

void ReInitI2C1() {
	HAL_I2C_DeInit(&hi2c1);
	HAL_I2C_Init(&hi2c1);
}

void WRITE_MCP(unsigned char Addr, uint8_t cmdByte, uint8_t LSBdata,
		uint8_t MSBdata) {
	uint8_t mcp_cmd[3];
	mcp_cmd[0] = cmdByte;
	mcp_cmd[1] = MSBdata;
	mcp_cmd[2] = LSBdata;
	HAL_StatusTypeDef stI2C = HAL_I2C_Master_Transmit(&hi2c1, Addr, mcp_cmd, 3,
			10000);
	if (stI2C != HAL_OK) {
//		GICS_DEBUGF_I2C("Write MCP failed, reinit");
		ReInitI2C1();
	}
}

void READ_MCP(unsigned char Addr, uint8_t cmdByte, uint8_t * pBuffer) {
	HAL_StatusTypeDef stI2C = HAL_I2C_Mem_Read(&hi2c1, Addr, cmdByte,
			I2C_MEMADD_SIZE_8BIT, pBuffer, 2, 10000);
	if (stI2C != HAL_OK) {
//		GICS_DEBUGF_I2C("Read MCP failed, reinit");
		ReInitI2C1();
	}
}

void gics_update_DAC_vars() {
	if (config_mode == GICS_IED) {
	PHASEINC = _50HzPHASEINC + (VDAC_CTRL[0] - 512) / 10;
	AMPLITUDE[0] = VDAC_CTRL[1] / 2; //Amplitudes V1=V4
	AMPLITUDE[1] = VDAC_CTRL[2] / 2;
	AMPLITUDE[2] = VDAC_CTRL[3] / 2;
	AMPLITUDE[3] = VDAC_CTRL[4] / 2;
	PHASEOFFSET[1] = 21845 + (VDAC_CTRL[5] - 512);

	AMPLITUDE[4] = VDAC_CTRL[6] / 2;
	AMPLITUDE[5] = VDAC_CTRL[6] / 2;
	AMPLITUDE[6] = VDAC_CTRL[6] / 2;
	// V4
	AMPLITUDE[7] = VDAC_CTRL[7] / 2;
	PHASEOFFSET[5] = 21845 + (VDAC_CTRL[5] - 512);
	}
}

void Update_DAC_Handler() {
	static uint16_t PHASE = 0;
	uint16_t X = 0;
	uint16_t V, A;
	uint16_t i;
	float XA;

	// phase accumulation
	PHASE = PHASE + PHASEINC;
	// tronqu� � 10 bits
	for (i = 0; i < N_DAC_CHANNELS; i++) {

		if (config_mode == GICS_IED) {
			A = ((PHASE + PHASEOFFSET[i]) >> 6) & 0x03FF;
			XA = (AMPLITUDE[i] * LTC1660LookupTable[A]);
			V = 512 + XA;
			V = V & 0x03FF;
			X = ((i + 1) << 12) + ((V << 2) & 0x0FFC);
		}
		if (config_mode == GICS_API)
			X = ((i + 1) << 12) +((VDAC_CTRL[i] <<2)  & 0x0FFC);
		/* Send Transaction data */
		SPI3_SS_GPIO_Port->BSRR = SPI3_SS_Pin << 16U;
		HAL_SPI_Transmit(&hspi3, (uint8_t*) &X, 1, HAL_MAX_DELAY);
		SPI3_SS_GPIO_Port->BSRR = SPI3_SS_Pin;
	}

}

void gics_init_DAC_vars() {
	for (size_t iLTC = 0; iLTC < 1024; iLTC++) {
		LTC1660LookupTable[iLTC] = sin(iLTC * 2.0 * 3.1415927 / 1024.0);
	}
	for (size_t iCh = 0; iCh < 8; iCh++) {
		AMPLITUDE[iCh] = 300;
	}
	AMPLITUDE[3] = 30;
	AMPLITUDE[7] = 30;
	PHASEOFFSET[0] = 0 * 21845;
	PHASEOFFSET[1] = 1 * 21845;
	PHASEOFFSET[2] = 2 * 21845;
	PHASEOFFSET[3] = 0 * 21845;

	PHASEOFFSET[4] = 0 * 21845;
	PHASEOFFSET[5] = 1 * 21845;
	PHASEOFFSET[6] = 2 * 21845;
	PHASEOFFSET[7] = 0 * 21845;
	PHASEINC = _50HzPHASEINC;
}
