#! /usr/bin/python3
#

import socket
import time
UDP_IP = "10.10.100.5"
UDP_PORT = 2015
#message_base = 0x06000000010000080008000800080008000800080008000e
message_base = 0x12000000010000080008000800080008000800080008000e
#MESSAGE=MESSAGE.to_bytes(24, 'big')
print("UDP target IP:", UDP_IP)
print("UDP target port:", UDP_PORT)


sock = socket.socket(socket.AF_INET, # Internet
                      socket.SOCK_DGRAM) # UDP
curLED=0
direction=1
iLoop=0
while(1):
    iLoop+=1
    #print("boucle %d"%iLoop)
    message=((message_base)&~(0xFFFF)) | (0x1 << curLED)
    message_bytes=message.to_bytes(24, 'big')
    #print("message:", message_bytes)
    sock.sendto(message_bytes, (UDP_IP, UDP_PORT))
    curLED+=direction
    if (curLED==0) | (curLED==15):
        direction*=-1
    time.sleep(0.0001)
    #time.sleep(0.1)
    
