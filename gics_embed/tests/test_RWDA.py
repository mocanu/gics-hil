#! /usr/bin/python3
#

import socket
import time
import struct
UDP_IP = "10.10.100.5"
UDP_PORT = 2015
#message_base = 0x06000000010000080008000800080008000800080008000e
message_base_W = 0x12000000010000080008000800080008000800080008000e
message_base_R = 0x11000000010000080008000800080008000800080008000e
#MESSAGE=MESSAGE.to_bytes(24, 'big')
print("UDP target IP:", UDP_IP)
print("UDP target port:", UDP_PORT)


sock = socket.socket(socket.AF_INET, # Internet
                      socket.SOCK_DGRAM) # UDP
sock.settimeout(1)
curLED=0
direction=1
iLoop=0
start = time.time()
ntimeout=0
while(1):
    try:
        iLoop+=1
        #print("boucle %d " % iLoop, end='')
        message=((message_base_W)&~(0xFFFF)) | (0x1 << curLED)
        message_bytes=message.to_bytes(24, 'big')
        #print("message:", message_bytes)
        sock.sendto(message_bytes, (UDP_IP, UDP_PORT))
        curLED+=direction
        if (curLED==0) | (curLED==15):
            direction*=-1
        message_bytes=message_base_R.to_bytes(24, 'big')
        #print("message:", message_bytes,len(message_bytes))
        sock.sendto(message_bytes, (UDP_IP, UDP_PORT))
        #print(" timeout=%d "%ntimeout, end='')
        #print(time.strftime("%d/%m/%Y %H:%M:%S")+" recv: ", end='')
        recv_bytes=sock.recvfrom(len(message_bytes))[0]
        #print(struct.unpack('12h', recv_bytes))
        #print()
        time.sleep(0.0001)
        #time.sleep(0.05)
    except socket.timeout as e:
        ntimeout+=1
        print(time.strftime("%d/%m/%Y %H:%M:%S"), end='')
        print(" timeout=%d "%ntimeout)
    
