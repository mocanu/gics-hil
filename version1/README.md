Programme de la carte G-ICS discovery interface
===============================================

Prérequis
---------

Pour la configuration de la carte (I/O, IRQ, ...), le logiciel STM32CubeMX v4.25 est utilisé.

Pour le développement logiciel, TrueSTUDIO for STM32 v9.0.0 est utilisé.

Programmation des cartes
------------------------

La compilation et le téléversement des programmes se fait depuis l'environnement de développement TrueStudio (Build puis Run ou Debug).

Avant de compiler le programme, il est nécessaire d'indiquer à la carte son identifiant pour que les addresses MAC et IP soient automatiquement configurées.
Cela se fait grâce à la variable GICS\_ID dans le fichier Inc\\gics.h

Architecture logicielle
-----------------------

Dans cette version, il n'y a pas d'OSTR intégré dans le projet.

La gestion des trames réseaux et la détection du mode local/distant se fait en tâche de fond.

L'envoi de données au DAC se fait lors de l'interruption timer3.

La lecture des voies de l'ADC1 se fait par balayage et écriture en mémoire par la DMA.

L'écriture des 16 sorties numériques se fait par un MCP23016 piloté par le bus I2C1.

La lecture des 16 entrées numériques se fait par un MCP23016 piloté par le bus I2C1.

Le DAC 16 voies (LTC1660) est piloté par le bus SPI3.

Fonctionnement selon le mode
----------------------------

En mode local:
 * les 8 entrées analogiques (via les potentiomètres) servent à contrôler les 8 sorties analogiques
 * les deux MCP23016 sont en entrée

En mode distant, les trames udp permettent de:
 * lire les entrées du second MCP23016 et de l'ADC
 * piloter les sorties du premier MCP23016 et contrôler les DAC

Procédure de validation
-----------------------

### Configuration lwip et ethernet ###

Vérifier que la carte réponde bien à l'ip 10.10.100.1+GICS\_ID et que son adresse mac soit 00:AB:CD:EF:04:07+GICS\_ID  (p.ex. avec wireshark).

### Etat des diodes ###

Les quatres diodes utilisateur de la carte STM32:
 * la diode rouge doit refléter l'étât local(allumée)/distant(éteinte)
 * la diode bleue s'inverse à chaque tour de la boucle de la tâche de fond
 * la diode orange est allumée pendant les interruptions timer3
 * la diode verte s'inverse toutes les 5000 interruptions timer3 (0.5s)

### Test des sorties analogiques ###

Au démarrage en mode distant:
 * la voie VA doit générer un sinus 0/4V à 50Hz.
 * la voie VB doit être en avance de 120°
 * la voie VC doit être en retard de 120°
 * la voie VD doit être en phase
 * idem pour les voies VE, VF, VG et VH
