/*
 * gics.c
 *
 *  Created on: 2 mai 2018
 *      Author: GICS
 */

#include "gics.h"
#include <math.h>

extern SPI_HandleTypeDef hspi3;
extern I2C_HandleTypeDef hi2c1;

void gics_udp_server_receive_callback(void *arg, struct udp_pcb *upcb, struct pbuf *p, const ip_addr_t *addr, u16_t port)
{

	struct pbuf * p2 = p;
	GICSTransaction * transaction;
	short *data;
	short function;
	short address;
	while(p2!=NULL)
	{//TODO:voir pour les incoherences dans la version originale
		GICS_DEBUGF("----------------------\r\n");
		GICS_DEBUGF("GICS: recv %d bytes from %s\r\n", p2->len, "?");
		transaction = (GICSTransaction*) (p2->payload);
		function=transaction->function;
		GICS_DEBUGF("Function : %d\r\n",transaction->function);
		GICS_DEBUGF("Count : %d\r\n",transaction->count);
		data=transaction->data;
		for(size_t iTransation=0; iTransation<transaction->count; iTransation++)
		{
			short localcount=0;
			address = transaction->address + iTransation;
			if (transaction->function & GICS_READ)
			{
				if (transaction->function & GICS_DISCRETE) {
					//		copier les  16 bits I dans data[0] et data[1]
					uint8_t a[2]={0xff,0xff};
					READ_MCP(0x42,0x00,a);
					data[0]=a[0];
					data[1]=a[1];
					localcount=transaction->count;
					GICS_DEBUGF("Read Discrete at %d: %d\n", address, data[iTransation]);
				}
				else if (transaction->function & GICS_ANALOG) {
					//	lire les 6 potantiometres (ADC) copier dans data[0] ...  data[7]
					//	conversion en continue et transfert par DMA il suffit de lire le tableau
					data[0]=ADCxDATA[0];
					data[1]=ADCxDATA[1];
					data[2]=ADCxDATA[2];
					data[3]=ADCxDATA[3];
					data[4]=ADCxDATA[4];
					data[5]=ADCxDATA[5];
					data[6]=ADCxDATA[6];
					data[7]=ADCxDATA[7];
					GICS_DEBUGF("Read Analog at %d: %d\n", address, data[iTransation]);
					localcount=transaction->count;
					break;
				}
				else if (transaction->function & GICS_DA){ /*** READ ANA and DISC */
					data[0]=ADCxDATA[0];
					data[1]=ADCxDATA[1];
					data[2]=ADCxDATA[2];
					data[3]=ADCxDATA[3];
					data[4]=ADCxDATA[4];
					data[5]=ADCxDATA[5];
					data[6]=ADCxDATA[6];
					data[7]=ADCxDATA[7];
					uint8_t a[2]={0xff,0xff};
					READ_MCP(0x42,0x00,a);
					GICS_DEBUGF("Digital input is: %x %x\r\n",a[1],a[0]);
					data[8]=a[0]*256+a[1];
					GICS_DEBUGF("Digital input is: %x\r\n",data[8]);
					localcount=9;
				}
				unsigned short tmp=transaction->count;
				transaction->count=localcount;
				GICS_DEBUGF("Frame dbg f: %d a: %d c:%d size:%d , \r\n",transaction->function, transaction->address,transaction->count,sizeof(GICSTransaction) - sizeof(transaction->data) + transaction->count * sizeof(short) );
				//error = socketSendTo(context->socket, &ipAddr, port, (const char *)&transaction, sizeof(transaction) - sizeof(transaction.data) + transaction.count * sizeof(short), NULL, 0);
				transaction->count=tmp;
			}
			else if(transaction->function & GICS_WRITE )
			{
				if((transaction->function & GICS_DA) | (transaction->function & GICS_ANALOG))
				{
					GICS_DEBUGF("Remote Analog Write");
					for(size_t iCh =0; iCh<8; iCh++)
					{
						VDAC_CTRL[iCh]=transaction->data[iCh];
					}
					if(gics_mode == GICS_MODE_REMOTE)
						gics_update_DAC_vars();

				}
				if((transaction->function & GICS_DISCRETE)|(transaction->function & GICS_DA))
				{
					GICS_DEBUGF("Write Discrete: %d\r\n",transaction->data[8]);
					//WRITE_MCP(0x40, 0x00, transaction->data[0], transaction->data[1]);
					WRITE_MCP(0x40,0x00,transaction->data[8]&0xFF,(transaction->data[8]&0xFF00)>>8);

				}
			}
		}
		p2=p->next;
	}
	if (function & GICS_READ)
	{ //on envoie une r�ponse qu'en mode lecture
		/* Connect to the remote client */
		udp_connect(upcb, addr, port);

		/* Tell the client that we have accepted it */
		udp_send(upcb, p);

		/* free the UDP connection, so we can accept new clients */
		udp_disconnect(upcb);
	}

  /* Free the p buffer */
  pbuf_free(p);
}

void gics_udp_server_init(void)
{
   struct udp_pcb *upcb;
   err_t err;

   /* Create a new UDP control block  */
   upcb = udp_new();

   if (upcb)
   {
     /* Bind the upcb to the UDP_PORT port */
     /* Using IP_ADDR_ANY allow the upcb to be used by any local interface */
      err = udp_bind(upcb, IP_ADDR_ANY, GICS_UDP_SERVER_PORT);

      if(err == ERR_OK)
      {
        /* Set a receive callback for the upcb */
        udp_recv(upcb, gics_udp_server_receive_callback, NULL);
      }
      else
      {
        udp_remove(upcb);
      }
   }
}

/*
 * Gestion du MCP23016:
 * L'adresse est cod�e sur 7 bits dans un mot de 8bits d�cal� � gauche (cf. doc HAL)
 * Le codage du mot est : 0b0100XYZ (X=A2, Y=A1, Z=A0)
 * donc:
 *  - le MCP en sortie est � l'adresse 0x40
 *  - le MCP en entr�e est � l'adresse 0x42
 */

void WRITE_MCP(unsigned char Addr, uint8_t cmdByte, uint8_t LSBdata, uint8_t MSBdata)
{
  uint8_t mcp_cmd[3];
  mcp_cmd[0]= cmdByte;
  mcp_cmd[1]= MSBdata;
  mcp_cmd[2]= LSBdata;
  HAL_StatusTypeDef stI2C = HAL_I2C_Master_Transmit(&hi2c1, Addr, mcp_cmd, 3, 10000);
  if(stI2C!=HAL_OK)
  {
	  GICS_DEBUGF_I2C("Write MCP failed, reinit");
	  HAL_I2C_DeInit(&hi2c1);
	  HAL_I2C_Init(&hi2c1);
  }
}

void READ_MCP(unsigned char Addr, uint8_t cmdByte, uint8_t * pBuffer)
{
	HAL_StatusTypeDef stI2C = HAL_I2C_Mem_Read(&hi2c1, Addr, cmdByte, I2C_MEMADD_SIZE_8BIT, pBuffer, 2, 10000);
	if(stI2C!=HAL_OK)
	{
		GICS_DEBUGF_I2C("Read MCP failed, reinit");
		HAL_I2C_DeInit(&hi2c1);
		HAL_I2C_Init(&hi2c1);
	}
}

void gics_update_DAC_vars()
{
	PHASEINC=_50HzPHASEINC+(VDAC_CTRL[0]-2048)/40;
	AMPLITUDE[0]=VDAC_CTRL[1]/8; //Amplitudes V1=V4
	AMPLITUDE[1]=VDAC_CTRL[2]/8;
	AMPLITUDE[2]=VDAC_CTRL[3]/8;
	AMPLITUDE[3]=VDAC_CTRL[4]/8;
	PHASEOFFSET[1]=21845+(VDAC_CTRL[5]-2048);

	AMPLITUDE[4]=VDAC_CTRL[6]/8;
	AMPLITUDE[5]=VDAC_CTRL[6]/8;
	AMPLITUDE[6]=VDAC_CTRL[6]/8;
	// V4
	AMPLITUDE[7]=VDAC_CTRL[7]/8;
	PHASEOFFSET[5]=21845+(VDAC_CTRL[5]-2048);
}

void Update_DAC_Handler()
{
	static uint16_t PHASE=0;
	uint16_t X= 0;
	uint16_t V,A;
	uint16_t i;
	float XA;

	// phase accumulation
	PHASE = PHASE + PHASEINC;
	// tronqu� � 10 bits
	for (i=0;i<N_DAC_CHANNELS;i++)
    {
		A=((PHASE+PHASEOFFSET[i]) >> 6) & 0x03FF;
		XA=(AMPLITUDE[i]*LTC1660LookupTable[A]);
		V=512+XA;
		V= V & 0x03FF;
		X=((i+1)<<12) + ((V << 2) & 0x0FFC);
		/* Send Transaction data */
		SPI3_SS_GPIO_Port->BSRR = SPI3_SS_Pin<<16U;
		HAL_SPI_Transmit(&hspi3, (uint8_t*)&X, 1, HAL_MAX_DELAY);
		SPI3_SS_GPIO_Port->BSRR = SPI3_SS_Pin;
	}

}

void gics_init_DAC_vars()
{
	for (size_t iLTC=0;iLTC<1024;iLTC++)
	{
		  LTC1660LookupTable[iLTC]=sin(iLTC*2.0*3.1415927/1024.0);
	}
	for (size_t iCh=0; iCh<8; iCh++)
	{
		AMPLITUDE[iCh]=500;
	}
    PHASEOFFSET[0]=0*21845;
    PHASEOFFSET[1]=1*21845;
    PHASEOFFSET[2]=2*21845;
    PHASEOFFSET[3]=0*21845;

    PHASEOFFSET[4]=0*21845;
    PHASEOFFSET[5]=1*21845;
    PHASEOFFSET[6]=2*21845;
    PHASEOFFSET[7]=0*21845;
    PHASEINC=_50HzPHASEINC;
}
